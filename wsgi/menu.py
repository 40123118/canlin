#coding: utf-8



# 第一層標題
m1 = "<a href='/about'>Brython程式區</a>"
# 第一層標題
m8 = "<a href='/introMember9'>solvespace零件</a>"
# 第一層標題
m7 = "<a href='/introMember8'>solvespace組立</a>"
# 第一層標題
m6 = "<a href='/introMember7'>SolidEdge零件</a>"
# 第一層標題
m0 = "<a href='/introMember0'>教學網站</a>"
# 第一層標題
m3 = "<a href=''>期末考四題</a>"
m31 = "<a href='/introMember10'>第一題</a>"
m32 = "<a href='/introMember11'>第二題</a>"
m33 = "<a href='/introMember12'>第三題</a>"
m34 = "<a href='/introMember13'>第四題</a>"
# 第一層標題
m4 = "<a href=''>第六組_組員介紹</a>"
m41 = "<a href='/introMember1'>吳羽閔-16</a>"
m42 = "<a href='/introMember2'>吳謦麟-18</a>"
m43 = "<a href='/introMember3'>林子航-22</a>"
m44 = "<a href='/introMember4'>林育民-23</a>"
m45 = "<a href='/introMember5'>戴志軒-53</a>"
# 第一層標題
m5 = "<a href='/introMember14'>附錄</a>"
# 請注意, 數列第一元素為主表單, 隨後則為其子表單
表單數列 = [ \
        [m1],  \
        [m8], \
        [m7], \
        [m6], \
        [m0], \
        [m3,m31,m32,m33,m34], \
        [m4, m41, m42, m43, m44,m45], \
        [m5]
        ]

def SequenceToUnorderedList(seq, level=0):
    # 只讓最外圍 ul 標註 class = 'nav'
    if level == 0:
        html_code = "<ul class='nav'>"
    # 子表單不加上 class
    else:
        html_code = "<ul>"
    for item in seq:
        if type(item) == type([]):
            level += 1
            html_code +="<li>"+ item[0]
            # 子表單 level 不為 0
            html_code += SequenceToUnorderedList(item[1:], 1)
            html_code += "</li>"
        else:
            html_code += "<li>%s</li>" % item
    html_code += "</ul>\n"
    return html_code

def GenerateMenu():
    return SequenceToUnorderedList(表單數列, 0) \
+'''
<script type="text/javascript" src="/static/jscript/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/static/jscript/dropdown.js"></script>
'''
