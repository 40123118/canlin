import cherrypy
import os

# 確定程式檔案所在目錄
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))

class HelloWorld(object):
    def index(self):
        return '''
<script src="/static/jscript/pfcUtils.js">
</script><script src="/static/jscript/wl_header.js">
document.writeln ("Error loading Pro/Web.Link header!");
</script><script language="JavaScript">
if (!pfcIsWindows()) netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
// 若第三輸入為 false, 表示僅載入 session, 但是不顯示
// ret 為 model open return
 var ret = document.pwl.pwlMdlOpen("cadp_block.prt", "c:/tmp", false);
if (!ret.Status) {
    alert("pwlMdlOpen failed (" + ret.ErrorCode + ")");
}
    //將 ProE 執行階段設為變數 session
    var session = pfcGetProESession();
    // 在視窗中打開零件檔案, 並且顯示出來
    var window = session.OpenFile(pfcCreate("pfcModelDescriptor").CreateFromFileName("cadp_block.prt"));
    var solid = session.GetModel("cadp_block.prt",pfcCreate("pfcModelType").MDL_PART);
    var d1,d2,myf,myn,i,j,volume,count,d1Value,d2Value;
    // 將模型檔中的 diameter 變數設為 javascript 中的 diameter 變數
    d1 = solid.GetParam("length");
    // 將模型檔中的 height 變數設為 javascript 中的 height 變數
    d2 = solid.GetParam("width");
//改變零件尺寸
    //myf=20;
    //myn=20;
    volume=0;
    count=0;
    try
    {
            // 以下採用 URL 輸入對應變數
            //createParametersFromArguments ();
            // 以下則直接利用 javascript 程式改變零件參數
            for(i=0;i<=3;i++)
            {
                //for(j=0;j<=2;j++)
                //{
                    myf=150;
                    myn=100+i*10;
// 設定變數值, 利用 ModelItem 中的 CreateDoubleParamValue 轉換成 Pro/Web.Link 所需要的浮點數值
         d1Value = pfcCreate ("MpfcModelItem").CreateDoubleParamValue(myf);
         d2Value = pfcCreate ("MpfcModelItem").CreateDoubleParamValue(myn);
// 將處理好的變數值, 指定給對應的零件變數
                    d1.Value = d1Value;
                    d2.Value = d2Value;
                    //零件尺寸重新設定後, 呼叫 Regenerate 更新模型
                    //solid.Regenerate(void null);
                    //利用 GetMassProperty 取得模型的質量相關物件
                    properties = solid.GetMassProperty(void null);
                    //volume = volume + properties.Volume;
volume = properties.Volume;
                    count = count + 1;
alert("執行第"+count+"次,零件總體積:"+volume);
// 將零件存為新檔案
var newfile = document.pwl.pwlMdlSaveAs("cadp_block.prt", "c:/tmp", "cadp_block_"+count+".prt");
if (!newfile.Status) {
    alert("pwlMdlSaveAs failed (" + newfile.ErrorCode + ")");
//}
                }
            }
            //alert("共執行:"+count+"次,零件總體積:"+volume);
            //alert("零件體積:"+properties.Volume);
            //alert("零件體積取整數:"+Math.round(properties.Volume));
        }
    catch(err)
        {
            alert ("Exception occurred: "+pfcGetExceptionType (err));
        }
</script>
'''
    index.exposed = True

    @cherrypy.expose
    def index2(self):
        return '''
<script src="/static/jscript/pfcUtils.js">
</script><script  src="/static/jscript/pfcParameterExamples.js"></script><script  src="/static/jscript/pfcComponentFeatExamples.js">
 document.writeln ("Error loading script!");
</script><script language="JavaScript">
      if (!pfcIsWindows())
        netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
  var session = pfcGetProESession ();
// for volume
  var solid = session.CurrentModel;
	try
		{
			createParametersFromArguments ();
                     solid.Regenerate(void null);   
                        properties = solid.GetMassProperty(void null);
                        alert("零件體積:"+properties.Volume);
		}
	catch (err)
		{
			alert ("Exception occurred: "+pfcGetExceptionType (err));
		}
</script>
'''
# 配合程式檔案所在目錄設定靜態目錄或靜態檔案
application_conf = {'/static':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/static"}
    }

#cherrypy.quickstart(HelloWorld(), config=application_conf)
application=cherrypy.Application(HelloWorld(), config=application_conf)